import os
import os.path
import random
import string
import argparse

parser = argparse.ArgumentParser(
    description="Securely remove files using Python3",
    epilog="By: tcincere [https://gitlab.com/tcincere]",
)

parser.add_argument(

    "-r", "--rounds",
    metavar="rounds",
    type=int,
    choices=range(3, 1000),
    help="How many times to iterate over file with random bytes and zeroes (default: 5)",
    default=5,
    dest="rounds",
    required=False,

)

parser.add_argument(

    "-e", "--erase",
    action="store_true",
    help="Erase file after rounds (default: False)",
    dest="erase",
    required=False,

)

parser.add_argument(

    "-f", "--file",
    metavar="file",
    type=str,
    help="File path",
    dest="file",
    required=True,

)

parser.add_argument(

    "-d", "--directory",
    metavar="directory",
    type=str,
    help="Path of directory which contains files to securely erase",
    dest="directory",
    required=False,

)

args = parser.parse_args()

# Check if the file path exists
def check_if_file(file: str) -> bool:
    file_exists: bool = False

    # Call OS function
    if os.path.exists(file):
        file_exists = True

    # Return bool value.
    return file_exists


# Function to remove file.
def remove_file(file: str) -> None:
    try:
        os.remove(file)
        print(f"\n[*] File removed successfully.\n")
    except OSError as e:
        print(f"\n[!] An error occured: {e}")


# Ask to remove file.
def ask_confirmation() -> bool:
    go_ahead: bool = False
    answer = input(f"\n[!] Are you sure you want to overwrite? (y/n) ")

    # Get first character of string
    answer = answer.lower()[0]

    if answer == "y":
        go_ahead = True
    elif answer == "n":
        # Exit if no
        print(f"[!] Secure removal terminated. Exiting...\n")
        exit(0)
    else:
        # Exit if wrong input, with appropriate message.
        print(f"[!] Wrong input! Exiting...\n")
    # print(answer)
    return go_ahead


# Try opening the file in binary mode (both read and write)
def open_file_and_overwrite(file: str):
    try:
        with open(file, 'rb+') as f:
            # Get file size
            file_size = os.path.getsize(file)

            if ask_confirmation():
                # Overwrite file with random bytes with user-supplied rounds
                for round in range(args.rounds):
                    print(f"[ROUND {round}] Overwriting with random data...")
                    f.seek(0)
                    f.write(os.urandom(file_size))

                # Final write of zeroes
                f.seek(0)
                print(f"\n[FINAL] Final write with zeroes...\n")
                f.write(b'\x00' * file_size)

                if args.erase:
                    remove_file(file)

    except Exception as error:
        print("An error has occurred when opening the file: {}", error)


def call_functions(file: str) -> None:
    is_file = check_if_file(file)

    if is_file:
        # Call function if file
        open_file_and_overwrite(file)
    else:
        # Exit if not file
        print("[ERROR] Not a file. Exiting...")
        exit(1)

# Call all other functions.
call_functions(args.file)