#!/usr/bin/env python3

import requests
import customtkinter as gui

# Set light mode for app
gui.set_appearance_mode("light")

# app elements
app = gui.CTk()
app.geometry("700x1080")
app.title("Lyrics Fetcher")

font_label = ("Inter", 36)
font_entries = ("Inter", 22)
font_button = ("Roboto", 32)

# Set frames
frame = gui.CTkFrame(master=app)
frame.pack(pady=30, padx=30, fill="both", expand=True)

# Set labels
label = gui.CTkLabel(master=frame, text="Lyrics Fetcher", font=font_label)
label.pack(pady=10, padx=10)

# Entries (song name and artist)
entry_song = gui.CTkEntry(master=frame, 
                          placeholder_text="Song", 
                          font=font_entries, 
                          width=400, 
                          height=60)

entry_song.pack(pady=10, padx=10)

# Author
entry_author = gui.CTkEntry(master=frame, 
                            placeholder_text="Artist", 
                            font=font_entries, 
                            width=400, 
                            height=60)
entry_author.pack(pady=10, padx=10)


def make_request(song_title, song_author) -> dict:
    url = f"https://lyrist.vercel.app/api/{song_title}/{song_author}"

    try:
        r = requests.get(url)
        r.raise_for_status()
        return r.json()

    except requests.exceptions.RequestException as e:
        print(f"Error: {e}")


def response():

    # Get data from gui fields
    title = entry_song.get()
    author = entry_author.get()

    # Check if title is empty
    if title:
        data = make_request(title, author)

        # Get lyrics from json data
        lyrics = data.get("lyrics", "Lyrics not found")
        lyrics_label.configure(text=lyrics)


# Fetch button
button = gui.CTkButton(master=frame, text="Fetch", command=response, font=font_button, height=65)
button.pack(pady=10, padx=10)

# Scrollable Frame for the result
scrollable_frame = gui.CTkScrollableFrame(master=frame, width=700, height=300)
scrollable_frame.pack(pady=10, padx=10, fill="both", expand=True)


# For outputing the results (lyrics), using the scrollable frame, as text will be long.
lyrics_label = gui.CTkLabel(master=scrollable_frame, text="Output", font=("Roboto", 22), wraplength=700, justify="center")
lyrics_label.pack(pady=10, padx=10)

app.mainloop()

