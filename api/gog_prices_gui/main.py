import requests
import json
from typing import Optional
from dateutil import parser

# Custom imports
import tables


def make_requests(game_id: int) -> dict:

    url = f"https://api.gog.com/products/{game_id}/prices?countryCode=gb"

    try:
        response = requests.get(url)
        response.raise_for_status()
        # print(type(response.json()))
        return response.json()
    except requests.exceptions.RequestException as e:
        print(f"Error: {e}")


def makeGameInfoRequest(game_id: int) -> dict:

    # URL for procuring game information
    # Basically copy/paste function
    url = f"https://api.gog.com/v2/games/{game_id}"

    try:
        response = requests.get(url)
        response.raise_for_status()
        # print(type(response.json()))
        return response.json()
    except requests.exceptions.RequestException as e:
        print(f"Error: {e}")


def makeBonusAndDlcRequest(game_id: int) -> dict:

    # URL for procuring bonus content information.
    # Basically copy/paste function
    url = f"https://api.gog.com/products/{game_id}?expand=downloads,expanded_dlcs"

    try:
        response = requests.get(url)
        response.raise_for_status()
        # print(type(response.json()))
        return response.json()
    except requests.exceptions.RequestException as e:
        print(f"Error: {e}")


# def makeGameFeaturesRequest(game_id: int) -> dict:

#     # URL for procuring bonus content information.
#     # Basically copy/paste function
#     url = f"https://api.gog.com/products/{game_id}?expand=downloads,expanded_dlcs"

#     try:
#         response = requests.get(url)
#         response.raise_for_status()
#         # print(type(response.json()))
#         return response.json()
#     except requests.exceptions.RequestException as e:
#         print(f"Error: {e}")


# Function to load the json file as a python dict
def loadJSONFile() -> dict:
    # Make try block
    with open("games.json", "r") as json_file:
        data = json.load(json_file)

    data = {key: int(value) for key, value in data.items()}
    # print(type(data['sands of time']))

    return data


# Function to write back to json file
def writeToJSONFile(data: dict) -> None:
    with open('games.json', 'w') as json_file:
        json.dump(data, json_file, indent=4)


# Loads then removes line from file then writes back.
def removeGameFromFile(game_id: int) -> None:

    result = {
        "success": False,
        "message": "",
    }

    # Load the file as python dict
    data = loadJSONFile()

    # Delete the game id line
    try:
        del data[game_id]
    except KeyError:
        result['message'] = "Game not found."
        return result

    # Write to file using function
    writeToJSONFile(data)

    # Load the file again to check if removed
    data = loadJSONFile()

    # Check if the value is in the data
    if game_id in data:
        # return function if not removed
        result['message'] = "Game NOT removed for some reason."
        return result

    # On success, return.
    result['success'] = True
    result['message'] = "Game successfully removed from file."

    return result


# Loads then adds game name and id and writes back to file.
def addGameToFile(game_title: str, game_id: int) -> None:

    result = {
        "success": False,
        "message": "",
    }

    # Load file using function
    data = loadJSONFile()

    # Check if user input is empty.
    if not game_title or not game_id:
        result['message'] = "No valid input, input field empty."
        return result

    # Check length of game_id
    # Must not exceed 10
    if len(game_id) > 10:
        result['message'] = "Game ID length is incorrect."
        return result

    # Check if ID contains any chars
    for char in game_id:
        if not char.isdigit():
            result['message'] = "Invalid type entered for game id."
            return result

    # Check if game already exists in file.
    # Convert game_id to int as dict is of type int
    if game_title in data or int(game_id) in data.values():
        result['message'] = "Game already exists in file."
        return result

    # If previous checks are a sucess then...
    # Add game to dict
    data[game_title] = game_id

    # Write to file using function
    writeToJSONFile(data)

    result['success'] = True
    result['message'] = "Game added to file."

    return result


# Parse for single game
def parse_data(data, game_title) -> str:
    # Store price data in data variable.

    tables.price_table.clear_rows()
    try:
        data = data["_embedded"]["prices"][0]
    except TypeError as e:
        if "NoneType" in str(e):
            return ""

    # Calculate percentage discount from base price.
    percentage_difference = calc_percentage_difference(
        int(data['basePrice'].split()[0]),
        int(data['finalPrice'].split()[0]),
    )

    # Convert each amount to GBP currency (e.g. £25.00)
    base_price = convert_to_money(data['basePrice'])
    final_price = convert_to_money(data['finalPrice'])

    # Check if game is on sale, if so, store relevant emoji.
    on_sale = final_price < base_price
    on_sale_emoji = "✅" if on_sale else "❌"

    game_fmt = game_title.upper()
    tables.price_table.add_row([game_fmt, final_price, base_price, percentage_difference, on_sale_emoji])

    return tables.price_table.get_string()


def parseGameInformation(data, game_title) -> dict:

    # Clear existing rows
    tables.game_info_table.clear_rows()

    # Dict for basic game information
    game_information = dict()

    # Try and access information stored in keys.
    try:
        game_information['Title'] = data['_embedded']['product']['title']
        game_information['Link'] = data['_links']['store']['href']
        game_information['Release'] = format_date(data['_embedded']['product']['globalReleaseDate'])
        game_information['GOG Release'] = format_date(data['_embedded']['product']['gogReleaseDate'])
        game_information['Publisher'] = data['_embedded']['publisher']['name']
        game_information['Studio'] = data['_embedded']['developers'][0]['name']
    except KeyError as e:
        # Handle key error
        # Return an error in dict, if fails
        print(e)
        game_information['Error'] = "Information unavailable"

    for key, value in game_information.items():
        tables.game_info_table.add_row([key, value])

    return tables.game_info_table.get_string()


def parseGameSystemRequirements(data, game_title) -> dict:

    tables.min_requirements_table.clear_rows()
    tables.optimal_requirements_table.clear_rows()

    # System requirements dict. Two indexs for minimum and optimal specs
    system_requirements = {
        0: list(),
        1: list(),
    }

    tables_list = list()

    # Iterate through each index
    for index in range(2):
        try:
            # Save each requirements list in a var
            requirements = data['_embedded']['supportedOperatingSystems'][0]['systemRequirements'][index][
                'requirements']
            # system_requirements[index] = requirements

            # Iterate through each requirement
            # Append it to the dict() and only append the description
            # e.g. processor, memory etc
            for spec in requirements:
                system_requirements[index].append(spec['description'])

            # Iterate through each requirement (minimum or optimal)
            for spec in system_requirements[index]:

                # If spec is 0 = minimum
                if index == 0:
                    # Add to minimum table
                    tables.min_requirements_table.add_row([spec])
                else:
                    # Add to optimal table
                    tables.optimal_requirements_table.add_row([spec])

        # Handle index error
        except IndexError as e:
            print(f"IndexError at index {index}: {e}")
            # Handle the case where there are no requirements at this index
            system_requirements[index] = "No requirements available"

        except KeyError as e:
            print(f"KeyError at index {index}: {e}")
            return "System requirements unavailable."

    # Append each table as a string to a list, to return
    tables_list.append(tables.min_requirements_table.get_string())
    tables_list.append(tables.optimal_requirements_table.get_string())

    return tables_list


def parseBonusAndDlcData(data) -> str:

    # Clear any existing rows.
    tables.bonus_dlc_table.clear_rows()

    try:
        bonus_dlc_data = data['downloads']['bonus_content']
    except KeyError as e:
        print(f"Error: {e}")

    if not bonus_dlc_data:
        tables.bonus_dlc_table.add_row(["No bonus content available.", "N/A", "N/A"])

    length = len(bonus_dlc_data)

    for index in range(length):
        tables.bonus_dlc_table.add_row(
            [
                f"{bonus_dlc_data[index]['name']}",
                f"{bonus_dlc_data[index]['type']}",
                f"{bonus_dlc_data[index]['count']}",
            ]
        )

    return tables.bonus_dlc_table.get_string()


def parseGameFeaturesData(data) -> str:

    # Clear any existing rows
    tables.features_table.clear_rows()
    # Access features within json
    try:
        features_data = data['_embedded']['features']

        # Need to measure how many features there are (as it may vary)
    except KeyError as e:
        print(e)
        tables.features_table.add_row(["No features available."])
        return tables.features_table.get_string()

    # Check if features data is present, if not return table
    # if not features_data:
    #     tables.features_table.add_row(["No game available."])
    #     return tables.features_table.get_string()

    # Variable to store features list
    length = len(features_data)

    # Iterate through each feature and access data.
    for index in range(0, length):

        # Add feature to the table
        tables.features_table.add_row([features_data[index]['name']])

    return tables.features_table.get_string()



def getAndParsePricesForDiscounted(title: str, game_id: int) -> Optional[str]:

    # Make request and store in variable.
    data = make_requests(game_id)

    # Store price data in data variable.
    try:
        data = data["_embedded"]["prices"][0]
    except TypeError as e:
        return e

    # Calculate percentage discount from base price.
    percentage_difference = calc_percentage_difference(
        int(data['basePrice'].split()[0]),
        int(data['finalPrice'].split()[0]),
    )

    # Convert each amount to GBP currency (e.g. £25.00)
    base_price = convert_to_money(data['basePrice'])
    final_price = convert_to_money(data['finalPrice'])

    # Check if game is on sale, if so, store relevant emoji.
    on_sale = final_price < base_price
    on_sale_emoji = "✅" if on_sale else "❌"

    game_fmt = title.upper()
    print(final_price, base_price)
    print(on_sale)

    if on_sale:
        tables.discounted_price_table.add_row([game_fmt, final_price, base_price, percentage_difference, on_sale_emoji])
        return tables.discounted_price_table.get_string()
    else:
        return None


def format_date(iso_date) -> str:

    try:
        datetime_object = parser.isoparse(iso_date)
        parsed_datetime = datetime_object.strftime("%B %d (%Y)")

        return parsed_datetime
    except ValueError as e:
        print(f"Error parsing key {iso_date}: {e}")
        return iso_date


def convert_to_money(price) -> float:
    return int(price.split()[0]) / 100


def calc_percentage_difference(base, final) -> int:
    difference = ""

    if base == final:
        difference = 0
    else:
        difference = ((base - final) / base) * 100

    # Round difference
    return round(difference)
