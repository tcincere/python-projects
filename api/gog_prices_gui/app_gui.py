#!/usr/bin/env python3

import customtkinter as gui

# Custom imports
import main

# Fonts
font_main = ("Consolas", 24)
font_cb = ("Consolas", 20)
font_combo = ("Consolas", 16)
font_entry = ("Helvetica", 18)
font_label = ("Helvetica", 20)
font_button = ("Roboto", 20)

checkbox_text = {
    "info": " 📕  Show game information",
    "bonus": " 🎄  Show bonus content and DLCs",
    "features": " 🚀  Show game features",
}

# Load the JSON file 'games.json'
gog_games = main.loadJSONFile()


def fetch_single_game_id() -> None:

    table_textbox.delete("1.0", gui.END)  # Clear the existing content

    # Get selection from combo box
    game_title = combo_box.get()

    # Get ID from dict
    game_id = gog_games[game_title]

    # Make request
    data = main.make_requests(game_id)

    # Parse JSON data
    game_table = main.parse_data(data, game_title)

    # Checks if the checkbox for additional game information is checked.
    if bool_info.get():
        # Make the request to get game information
        data = main.makeGameInfoRequest(game_id)

        # Return the game info table
        game_info_table = main.parseGameInformation(data, game_id)

        # Insert the table into the text box
        table_textbox.insert(gui.END, game_info_table)

        # Returns a list of requirements from index 0 to 1
        # 0 = minimum requirements
        # 1 = optimal requirements
        requirements_tables_list = main.parseGameSystemRequirements(data, game_id)

        # Insert each table into the textbox
        table_textbox.insert(gui.END, requirements_tables_list[0])
        table_textbox.insert(gui.END, requirements_tables_list[1])

    # Check if feature checkbox is selected
    if bool_features.get():
        print("[X] Feature selected")
        # Use the request to parse the data
        data = main.makeGameInfoRequest(game_id)
        features_table = main.parseGameFeaturesData(data)

        # Print the features table.
        table_textbox.insert(gui.END, features_table)

    # Checks if the checkbox for bonus content is checked.
    if bool_bonus.get():
        print("[X] Bonus selected")
        data = main.makeBonusAndDlcRequest(game_id)
        bonus_table = main.parseBonusAndDlcData(data)

        # Insert the table into the text box
        table_textbox.insert(gui.END, bonus_table)

    # Update the textbox with the parsed table data
    table_textbox.insert(gui.END, game_table)  # Insert the new table content


def fetchDiscountedGamesOnly() -> None:

    discounted_table_textbox.delete("1.0", gui.END)  # Clear the existing content

    # Accumulate all discounted game data
    discounted_games_list = []

    for title, game_id in gog_games.items():
        # Get discounted data as a string
        # Returns either a String or None type.
        discounted = main.getAndParsePricesForDiscounted(title, game_id)

        # Ensure the discounted data is a string and accumulate it
        if discounted is None:
            continue
        else:
            discounted_games_list.append(discounted)

    # Insert the concatenated data into the textbox
    # Print last element of list (final table) to gui textbox
    discounted_table_textbox.insert(gui.END, discounted_games_list[-1])


def removeGame() -> None:
    game_id = remove_combo_box.get()
    result = main.removeGameFromFile(game_id)
    message = result['message']

    title_add_remove.configure(text=message)
    print(result)


def addGame() -> None:
    # Get entries from text box
    game_title = add_entry_game.get().lower()
    game_id = add_entry_game_id.get()

    # Store result -> dict of bool (sucess/error) and corresponding message.
    result = main.addGameToFile(game_title, game_id)
    message = result['message']

    title_add_remove.configure(text=message)
    print(result)


# Create main app window
app = gui.CTk()
app.geometry("1920x1080")
app.title("Check GOG Game Prices")

my_tab = gui.CTkTabview(app, corner_radius=20)
my_tab.pack(pady=5)

# Create tabs
tab_single_game = my_tab.add("Single Game")
tab_discounted_game = my_tab.add("Discounted Games")
tab_add_remove_game = my_tab.add("Add or Remove Games")

# def getBoolForInfo() -> bool:
#     tab_single_game.get()

# Boolean values for checkboxes
bool_info = gui.BooleanVar()
bool_bonus = gui.BooleanVar()
bool_features = gui.BooleanVar()

# Create checkboxes
cb_info = gui.CTkCheckBox(
    master=tab_single_game,
    text=checkbox_text['info'],
    variable=bool_info,
    onvalue=True,
    offvalue=False,
    font=font_label,
)

cb_info.pack(pady=10)

cb_bonus = gui.CTkCheckBox(
    master=tab_single_game,
    text=checkbox_text['bonus'],
    variable=bool_bonus,
    onvalue=True,
    offvalue=False,
    font=font_label,
)

cb_bonus.pack(pady=10)

cb_features = gui.CTkCheckBox(
    master=tab_single_game,
    text=checkbox_text['features'],
    variable=bool_features,
    onvalue=True,
    offvalue=False,
    font=font_label,
)

cb_features.pack(pady=10)


# tab_single_game - START
# ---------------------------------------------------------------------------

# Title for combo box - tab_single_game
output_for_combo = gui.CTkLabel(
    master=tab_single_game,
    text="List of Games",
    font=font_label,
)

output_for_combo.pack(pady=10, padx=10)

# Create a combo box to list games by id - tab_single_game
combo_box = gui.CTkComboBox(
    master=tab_single_game,
    values=list(gog_games.keys()),
    font=font_entry,
    width=300,
    height=40,
    dropdown_font=font_entry
)
combo_box.pack(pady=10, padx=10)

# Button for combo box - tab_single_game
combo_button = gui.CTkButton(
    master=tab_single_game,
    text="Fetch",
    command=fetch_single_game_id,
    width=60,
    height=50,
    font=font_button,
    corner_radius=15
)

combo_button.pack(pady=20, padx=50)

# Show table - tab_single_game
table_textbox = gui.CTkTextbox(
    master=tab_single_game,
    width=1300,
    height=800,
    font=font_combo
)
table_textbox.pack(pady=50, padx=50)

# tab_single_game - END
# ---------------------------------------------------------------------------



# tab_discounted_game - START
# ---------------------------------------------------------------------------
# Title for combo box - tab_discounted_game
title_discounted = gui.CTkLabel(
    master=tab_discounted_game,
    text="GOG Discounted Games",
    font=font_entry,
)

title_discounted.pack(pady=10, padx=10)


# Button for discounted - tab_discounted_game
discounted_button = gui.CTkButton(
    master=tab_discounted_game,
    text="Fetch",
    command=fetchDiscountedGamesOnly,
    width=60,
    height=50,
    font=font_button,
    corner_radius=15,
)

discounted_button.pack(pady=20, padx=50)

# Show table - tab_discounted_game
discounted_table_textbox = gui.CTkTextbox(
    master=tab_discounted_game,
    width=1000,
    height=800,
    font=font_combo
)

discounted_table_textbox.pack(pady=50, padx=50)

# tab_discounted_game - END
# ---------------------------------------------------------------------------




# tab_add_remove_game - START
# ---------------------------------------------------------------------------
# Title for combo box - tab_discounted_game
title_add_remove = gui.CTkLabel(
    master=tab_add_remove_game,
    text="Add or remove a game from the list",
    font=font_label
)

title_add_remove.pack(pady=10, padx=10)

add_entry_game = gui.CTkEntry(
    master=tab_add_remove_game,
    placeholder_text="Enter game name",
    font=font_entry,
    height=50,
    width=400,
)

add_entry_game_id = gui.CTkEntry(
    master=tab_add_remove_game, 
    placeholder_text="Enter game id", 
    font=font_entry,
    height=50,
    width=400,
)

add_entry_game.pack(pady=10, padx=10)
add_entry_game_id.pack(pady=10, padx=10)

# Button for adding game - tab_add_remove_game
add_button = gui.CTkButton(
    master=tab_add_remove_game,
    text="Add",
    command=addGame,
    width=60,
    height=50,
    font=font_button,
    corner_radius=10,
)


# Button for removing game - tab_add_remove_game
remove_button = gui.CTkButton(
    master=tab_add_remove_game,
    text="Remove",
    command=removeGame,
    width=60,
    height=50,
    font=font_button,
    corner_radius=10,
)

# Create a combo box to list games by id - tab_add_remove_game
remove_combo_box = gui.CTkComboBox(
    master=tab_add_remove_game,
    values=list(gog_games.keys()),
    font=font_entry,
    width=300, 
    height=40, 
    dropdown_font=font_entry)

# Pack elements
add_button.pack(pady=20, padx=50)
remove_combo_box.pack(pady=10, padx=10)
remove_button.pack(pady=20, padx=50)


# Show table - tab_add_remove_game
# discounted_table_textbox = gui.CTkTextbox(master=tab_add_remove_game, width=1000, height=800, font=font_combo)
# discounted_table_textbox.pack(pady=50, padx=50)

# tab_add_remove_game - END
# ---------------------------------------------------------------------------

app.mainloop()
