from prettytable import PrettyTable

price_table = PrettyTable()
price_table.field_names = [
    "Title",
    "Sale Price",
    "Base Price",
    "Discount(%)",
    "On Sale?"
]

discounted_price_table = PrettyTable()
discounted_price_table.field_names = [
    "Titles on sale",
    "Sale Price",
    "Price",
    "Discount(%)",
    "On Sale?"
]

game_info_table = PrettyTable()
game_info_table.field_names = [
    "Type",
    "Information",
]

game_info_table.align['Type'] = "l"
game_info_table.align['Information'] = "l"

min_requirements_table = PrettyTable()
min_requirements_table.field_names = [
    "Minimum",
]
min_requirements_table.align['Minimum'] = "l"

optimal_requirements_table = PrettyTable()
optimal_requirements_table.field_names = [
    "Optimal",
]

optimal_requirements_table.align['Optimal'] = "l"

features_table = PrettyTable()
features_table.field_names = [
    "Features",
]

bonus_dlc_table = PrettyTable()
bonus_dlc_table.field_names = [
    "Bonus Content",
    "Type",
    "Count",
]
