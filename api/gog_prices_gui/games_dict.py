#!/usr/bin/env python3

# GOG games list by id.

from colorama import Fore, init


gog_games_by_id = {
    "baldurs gate 3": 1456460669,
    "civ 4": 1760534591,
    "witcher 3 expansion": 1427195509,
    "ghostrunner dlc": 1443435189,
    "ghostrunner 2": 1825685277,
    "indika": 1366870568,
    "mafia": 1993581340,
    "mafia 2": 1943447848,
    "manor lords": 1361243432,
    "phantom liberty": 1256837418,
    "plague tale innocence": 1901367087,
    "plague tale requiem": 1552771812,
    "plague tale bundle": 1879129489,
    "robocop": 1950574400,
    "severed steel": 1242122770,
    "sands of time": 1207658747,
    "trinity fusion": 2090070295,
    "uncharted": 1451150270,
}


def list_games(games_dict: dict) -> None:

    from prettytable import PrettyTable
    game_id_table = PrettyTable()
    game_id_table.field_names = ["Title", "Unique Identifier"]

    init(autoreset=True)
    for key, value in games_dict.items():
        game_id_table.add_row([f"{Fore.BLUE}{key.upper()}{Fore.WHITE}", f"{Fore.YELLOW}{value}{Fore.WHITE}"])

    if len(game_id_table.rows) > 0:
        print(game_id_table)
