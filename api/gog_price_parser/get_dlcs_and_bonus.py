#!/usr/bin/env python3

import requests
from prettytable import PrettyTable

# Custom imports
import games_dict
import my_arg_parse


def check_all_games_dlcs_and_bonus():
    print("Getting bonus content and dlc data...")

    for game in games_dict.gog_games_by_id:
        url = f"https://api.gog.com/products/{games_dict.gog_games_by_id[game]}?expand=downloads,expanded_dlcs"
        raw_data = make_bonus_and_dlc_request(url)
        parse_bonus_and_dlc_data(raw_data, game)


def check_discounted_games_dlcs_and_bonus(discounted_games):
    # Using list comprehension to flatten the list
    new_discounted_list = [item for sublist in discounted_games for item in sublist]

    for discounted_game in new_discounted_list:
        url = f"https://api.gog.com/products/{games_dict.gog_games_by_id[discounted_game]}?expand=downloads,expanded_dlcs"
        raw_data = make_bonus_and_dlc_request(url)
        parse_bonus_and_dlc_data(raw_data, discounted_game)


def check_single_games_dlcs_and_bonus():
    print("Getting bonus content and dlc data...")
    game = my_arg_parse.game_to_check
    game_id = games_dict.gog_games_by_id[game]
    url = f"https://api.gog.com/products/{game_id}?expand=downloads,expanded_dlcs"
    raw_data = make_bonus_and_dlc_request(url)
    parse_bonus_and_dlc_data(raw_data, game)


def make_bonus_and_dlc_request(formatted_url):
    try:
        response = requests.get(formatted_url)
        response.raise_for_status()
        return response.json()
    except requests.exceptions.RequestException as e:
        print(f"Error: {e}")
        exit(1)


def parse_bonus_and_dlc_data(bonus_and_dlc_json_data, game_title):
    try:
        bonus_data = bonus_and_dlc_json_data['downloads']['bonus_content']
    except IndexError as e:
        print(f"Error: {e}")
        exit(1)

    if bonus_data:
        # Create table.
        bonus_content_table = PrettyTable()
        #bonus_content_table.header(game_title)
        bonus_content_table.field_names = [f"({game_title.upper()}) Bonus Content", "Type", "Count"]

        for index in range(len(bonus_data)):
            bonus_content_table.add_row(
                [f"{bonus_data[index]['name']}",
                 f"{bonus_data[index]['type']}",
                 f"{bonus_data[index]['count']}"]
            )
            #print(f'Name: {bonus_data[index]['name']}, Type: {bonus_data[index]['type']}, Count: {bonus_data[index]['count']}\n')

        if len(bonus_content_table.rows) != 0:
            print(bonus_content_table)
            from time import sleep as s
            s(2)
    else:
        print(f"{game_title.upper()}: No additional content found.")
