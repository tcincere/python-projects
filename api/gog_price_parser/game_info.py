from dateutil import parser
from colorama import Fore
from prettytable import PrettyTable


def parse_game_info_data(info_data) -> dict:
    game_link = info_data['_links']['store']['href']
    game_product = info_data['_embedded']['product']
    game_title = game_product['title']
    global_release_date = game_product['globalReleaseDate']
    gog_release_date = game_product['gogReleaseDate']
    publisher = info_data['_embedded']['publisher']['name']
    developers = info_data['_embedded']['developers'][0]['name']

    system_requirements_dict: list = parse_system_requirements(info_data)
    game_features: list = parse_game_features(info_data)
    # print(game_features)

    parsed_game_info = {
        "Title": game_title,
        "Link": game_link,
        "Global Release": global_release_date,
        "GOG Release": gog_release_date,
        "Publisher": publisher,
        "Studio": developers,
    }

    full_game_info_dict = dict()
    full_game_info_dict['game_info'] = parsed_game_info
    full_game_info_dict['system_requirements'] = system_requirements_dict
    full_game_info_dict['features'] = game_features

    return full_game_info_dict


def parse_system_requirements(info_data) -> dict:
    # Create empty list.
    spec_requirements = dict()
    spec_requirements[0] = list()
    spec_requirements[1] = list()

    # Ignore these requirements
    ignore_keys = ['DirectX:', 'Other:', 'Additional Features:', 'System:']

    for index in range(0, 2):
        try:
            requirements_temp = info_data['_embedded']['supportedOperatingSystems'][0]['systemRequirements'][index][
                'requirements']
        except IndexError:
            print(f"\nSystem requirements unavailable for this game.")
            continue
            # exit(0)

        for spec in requirements_temp:
            if spec['name'] not in ignore_keys:
                spec_requirements[index].append(spec['description'])

    return spec_requirements


def parse_game_features(info_data) -> list:
    # Access features within json
    try:
        raw_features_data = info_data['_embedded']['features']

        # Need to measure how many features there are (as it may vary)
        raw_features_length = len(raw_features_data)
    except IndexError:
        print(f"\n Features data for this game are unavailable.")

    # Variable to store features list
    features = list()

    # Iterate through each feature and access data.
    for index in range(0, raw_features_length):
        temp_var = raw_features_data[index]

        # Add each feature to the list
        features.append(temp_var['name'])

    return features


def format_date(iso_date) -> str:
    try:
        datetime_object = parser.isoparse(iso_date)
        parsed_datetime = datetime_object.strftime("%B %d (%Y)")

        return parsed_datetime
    except ValueError as e:
        print(f"Error parsing key {iso_date}: {e}")
        # return iso_date


def print_parsed_info_data(full_game_info) -> None:
    date_keys = ["Global Release", "GOG Release"]

    # Variable to reset colours.
    rst = Fore.WHITE
    # Store each data set into its own variable.
    game_info = full_game_info['game_info']
    requirements = full_game_info['system_requirements']
    features = full_game_info['features']

    # Configre basic game information
    game_info_table = PrettyTable()
    features_table = PrettyTable()

    game_info_table.field_names = ['Info', 'Data']
    game_info_table.align['Info'] = "l"
    game_info_table.align['Data'] = "l"

    for info, data in game_info.items():
        if info in date_keys:
            # Parse the date so it's actually readable
            game_info_table.add_row([f"{Fore.BLUE}{info}{rst}", f"{Fore.RED}{format_date(data)}{rst}"])
        else:
            game_info_table.add_row([f"{Fore.BLUE}{info}{rst}", f"{Fore.RED}{data}{rst}"])

    print(f"\nFurther information")
    # Configure specification tables.
    requirements_table = PrettyTable()

    requirements_table.field_names = ['Minimum', 'Optimal']
    requirements_table.align['Minimum'] = "l"
    requirements_table.align['Optimal'] = "l"

    # Get length of longest list.
    max_length = max(len(requirements[0]), len(requirements[1]))

    for i in range(max_length):

        min_spec = ""
        opt_spec = ""

        # Get min/optc spec for current index, or empty string if index is out of range.
        if i < len(requirements[0]):
            min_spec = requirements[0][i]

        if i < len(requirements[1]):
            opt_spec = requirements[1][i]

        # Add row to the table.
        requirements_table.add_row([f"{Fore.MAGENTA}{min_spec}{rst}", f"{Fore.GREEN}{opt_spec}{rst}"])

    # Configure features table.
    features_table.field_names = ["Features"]

    # Left align
    features_table.align["Features"] = "l"

    features = ", ".join(features)
    features_table.add_row([f"{Fore.YELLOW}{features}{rst}"])

    # Print all tables.
    print(game_info_table)
    print(requirements_table)
    print(features_table)
