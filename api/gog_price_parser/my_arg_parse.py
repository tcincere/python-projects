import argparse

parser = argparse.ArgumentParser(
    description="Get GOG game prices",
    epilog="By: tcincere [https://gitlab.com/tcincere]",
)

parser.add_argument(
    "-g", "--game",
    metavar="game",
    type=str,
    default="all",
    help="Name of game",
    dest="game",
    required=False,
)

parser.add_argument(
    "-d", "--discounted",
    action="store_true",
    help="Only show discounted games",
    dest="discounted",
    required=False,
)

parser.add_argument(
    "-i", "--info",
    action="store_true",
    help="Show game info (weblink, system requirements, publisher, etc",
    dest="info",
    required=False,
)

parser.add_argument(
    "-b", "--bonus",
    action="store_true",
    help="Obtain additional info (expansions, dlcs and bonus content)",
    dest="bonus_and_dlc",
    required=False,
)

parser.add_argument(
    "-l", "--list-games",
    action="store_true",
    help="List all the games stored in the games dict",
    dest="list_games",
    required=False,
)

args = parser.parse_args()
game_to_check = args.game.lower()
discounted = args.discounted
info = args.info
list_games = args.list_games
bonus_and_dlc = args.bonus_and_dlc
