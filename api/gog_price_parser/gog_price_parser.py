#!/usr/bin/env python3

import requests
from colorama import Fore, init
from prettytable import PrettyTable
from typing import Union, List

# Custom imports
import my_arg_parse
import game_info
import games_dict
import get_dlcs_and_bonus as dlc

# Initialize colorama
init(autoreset=True)

# Create tables for all game prices and a table for discounted games only.
price_table = PrettyTable()
price_table.field_names = ["Titles", "Price", "Sale Price", "Discount(%)", "On Sale?"]

discounted_price_table = PrettyTable()
discounted_price_table.field_names = ["Titles on sale", "Sale Price", "Discount(%)", "Price"]


# Base functions for making api requests.
def make_requests(url) -> dict:
    try:
        response = requests.get(url)
        response.raise_for_status()
        # print(type(response.json()))
        return response.json()
    except requests.exceptions.RequestException as e:
        print(f"Error: {e}")


def check_all_games() -> Union[List, None]:
    print("Getting data...")
    d_games = list()
    # Iterate through each game.
    for game in games_dict.gog_games_by_id:
        # Make request to title by id
        url = f"https://api.gog.com/products/{games_dict.gog_games_by_id[game]}/prices?countryCode=gb"

        # Parse the data and check discounted variable, to show only discounted games.
        d_temp = parse_json_data(make_requests(url), game, my_arg_parse.discounted)
        if d_temp:
            d_games.append(d_temp)

        # Check if system requirements variable to show those too.
        if my_arg_parse.info and not my_arg_parse.discounted:
            info_url = f"https://api.gog.com/v2/games/{games_dict.gog_games_by_id[game]}"
            game_info.print_parsed_info_data(game_info.parse_game_info_data(make_requests(info_url)))

        if my_arg_parse.discounted and d_temp and my_arg_parse.info:
            info_url = f"https://api.gog.com/v2/games/{games_dict.gog_games_by_id[d_temp[0]]}"
            game_info.print_parsed_info_data(game_info.parse_game_info_data(make_requests(info_url)))

    # Check length of table rows. Don't print if not necessary.
    if my_arg_parse.discounted and len(discounted_price_table.rows) > 0:
        print(discounted_price_table)
        return d_games

    # Don't print whole table if not discounted
    if len(price_table.rows) > 0 and not my_arg_parse.discounted:
        print(price_table)


def check_single_game() -> None:
    # Check a single game by game title
    if check_game_is_valid():
        url = f"https://api.gog.com/products/{games_dict.gog_games_by_id[my_arg_parse.game_to_check]}/prices?countryCode=gb"
        parse_json_data(make_requests(url), my_arg_parse.game_to_check, my_arg_parse.discounted)

        # Print price table.
        print(price_table)

        if my_arg_parse.info:
            info_url = f"https://api.gog.com/v2/games/{games_dict.gog_games_by_id[my_arg_parse.game_to_check]}"
            game_info.print_parsed_info_data(game_info.parse_game_info_data(make_requests(info_url)))
    else:
        # Handle error if game is not found within dictionary.
        print(f"[!] Error. {my_arg_parse.game_to_check} not in list.")


# Check the game actually exists within the games dictionary
# These values have to be hard-coded for now, until a better solution is found.
def check_game_is_valid() -> bool:
    game_valid = False

    if my_arg_parse.game_to_check in games_dict.gog_games_by_id:
        game_valid = True

    return game_valid


rst = Fore.WHITE

from typing import Union, List


def parse_json_data(data, game_title, discounted) -> Union[List, None]:
    # Store price data in data variable.
    try:
        data = data["_embedded"]["prices"][0]
    except TypeError:
        print(f"{Fore.RED}Type error: data could not be obtained")
        exit(1)

    # Calculate percentage discount from base price.
    percentage_difference = calc_percentage_difference(
        int(data['basePrice'].split()[0]),
        int(data['finalPrice'].split()[0]),
    )

    # Convert each amount to GBP currency (e.g. £25.00)
    base_price = convert_to_money(data['basePrice'])
    final_price = convert_to_money(data['finalPrice'])

    # Check if game is on sale, if so, store relevant emoji.
    on_sale = final_price < base_price
    on_sale_emoji = "✅" if on_sale else "❌"

    # Formatted title and price with colours for easier readability.
    fmt_game_title = f"{Fore.YELLOW}{game_title}{rst}"
    fmt_final_price = f"{Fore.GREEN}£{final_price}{rst}"
    fmt_base_price = f"{Fore.RED}£{base_price}{rst}"
    fmt_percent_off = f"{Fore.MAGENTA}{percentage_difference}%{rst}"

    discounted_games_list = list()

    if my_arg_parse.discounted and final_price < base_price:
        discounted_games_list.append(game_title)
        discounted_price_table.add_row([fmt_game_title, fmt_final_price, fmt_percent_off, fmt_base_price])
    else:
        price_table.add_row([fmt_game_title, fmt_base_price, fmt_final_price, fmt_percent_off, on_sale_emoji])

    if discounted_games_list:
        return discounted_games_list


def convert_to_money(price) -> float:
    return int(price.split()[0]) / 100


def calc_percentage_difference(base, final) -> int:
    difference = ""

    if base == final:
        difference = 0
    else:
        difference = ((base - final) / base) * 100

    # Round difference
    return round(difference)


# game_url = https://api.gog.com/v2/games/2049187585

# Call main function based on argparse parameters using conditions.
if my_arg_parse.list_games:
    games_dict.list_games(games_dict.gog_games_by_id)
    exit(0)
elif my_arg_parse.game_to_check == "all" and not my_arg_parse.discounted:
    check_all_games()
    if my_arg_parse.bonus_and_dlc:
        dlc.check_all_games_dlcs_and_bonus()

elif my_arg_parse.discounted:
    discounted_games = check_all_games()
    if my_arg_parse.bonus_and_dlc and discounted_games:
        dlc.check_discounted_games_dlcs_and_bonus(discounted_games)

elif my_arg_parse.game_to_check != "all":
    check_single_game()
    if my_arg_parse.bonus_and_dlc:
        dlc.check_single_games_dlcs_and_bonus()

