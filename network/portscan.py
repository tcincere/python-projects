#!/usr/bin/env python3

import socket
import argparse
import nmap3
from datetime import datetime

# Colour definitions.
RED   = "\033[1;31m"  
YW    = "\033[1;93m"
CYAN  = "\033[1;36m"
GREEN = "\033[1;32m"
RST   = "\033[0;0m"

parser = argparse.ArgumentParser(

    description="Simple python port scanner with nmap functionality",
    epilog="by: tcincere [https://gitlab.com/tcincere]"
)

parser.add_argument(
    "-i", "--ip-address",
    type=str,
    help="IP address to scan",
    required=True
)

parser.add_argument(
    "-p", "--ports",
    type=str,
    help="Enter port(s) to scan",
    required=False
)

parser.add_argument(
    "-t", "--timeout",
    help="Set timeout before connection reset, default 5",
    type=int,
    required=False,
    default=5
)

args = parser.parse_args()
ip = args.ip_address
timeout = args.timeout

# Set the TCP socket and timeout
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.settimeout(timeout)

# Set nmap scanner
nmap = nmap3.NmapScanTechniques()


ports = [i for i in range(65536)]
open_ports = []


def printHeaders():

    # Remove everything after "." in date object.
    time = str(datetime.now()).split(".", 1)[0]

    print(f"\n{YW}Portscanner: {RED}by tcincere")
    print(f"{YW}Scan date: {CYAN}{time}{RST}")
    print(f"=========================================\n")


def checkStatus(ip):

    ip_status = nmap.nmap_ping_scan(ip).get(ip)

    if ip_status is None:
        print(f"{RED}[!] Invalid IP address.")
        exit(1)

    status = ip_status.get('state').get('state')
    reason = ip_status.get('state').get('reason')

    machine_status = [status, reason]
    return machine_status


def scan(ip):
    printHeaders()
    machine_status = checkStatus(ip)
    print(f"{YW}IP: {CYAN}\t{ip}")

    if 'down' in machine_status:
        print(f"{YW}Status:{RED} {machine_status[0]}")
        print(f"{YW}Reason:{RED} {machine_status[1]}")
        exit(0)
    elif 'up' in machine_status:
        print(f"{YW}Status:{GREEN} {machine_status[0]}{RST}\n")

    for port in ports:
        if sock.connect_ex((ip, port)):
            continue
        else:
            print(f"{GREEN}[+] Port {port} is open{RST}")
            open_ports.append(port)

    if not open_ports:
        print(f"{RED}[!] No open ports found{RST}")
    else:
        print()
        print(*open_ports, sep=",")


scan(ip)