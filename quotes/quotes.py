#!/usr/bin/env python3

from json import loads
from random import randrange
import argparse

parser = argparse.ArgumentParser(
    description="Quotes.py: print random quotes from json file",
    epilog="By: tcincere [https://gitlab.com/tcincere]",
)

parser.add_argument(

    "-l", "--length",
    metavar="length",
    type=int,
    choices=range(6, 31),
    help="Length of quote (number of words in quote, less than or equal to)",
    default=12,
    dest="quote_length",
    required=False,

)

args = parser.parse_args()

# Re-name variable
quote_length = args.quote_length

RED = "\033[1;31m"
YW = "\033[0;93m"
CYAN = "\033[0;36m"
GREEN = "\033[0;32m"
MAGENTA = "\033[0;35m"
RST = "\033[0;0m"


def open_file() -> str:
    # Try opening file, else print and exit with error.
    try:
        with open('quotes.json', 'r') as f:
            json_output = f.read()
            return loads(json_output)

    except FileNotFoundError:
        print(f"{RED}[ERROR] File not found!")
        exit(1)

    except Exception as err:
        print(f"{RED}[ERROR] An error has occurred!", err)
        exit(1)


def get_string() -> str:
    return open_file()


def get_quote(json_str: str) -> dict:
    # Create random number
    random_number = randrange(0, 1623)

    # Get random quote from set, depending on randon number
    quote = json_str[random_number]

    # Get only text (quote)
    quote_text = quote['text']

    # Split to count how many words there are.
    quote_length_by_word = len(quote_text.split())

    # Create into dictionary
    quote_info = {"length_in_words": quote_length_by_word,
                  "text": quote_text,
                  "author": quote['author']}

    # Return info
    return quote_info


def get_quote_by_length() -> None:
    # Open file and read quotes into json string
    json_str = open_file()

    while True:
        # Call function and pass quotes as string from memory
        q = get_quote(json_str)

        # If quote_length argument is set
        if quote_length:
            # Find quotes less than or equal to specified length.
            if q["length_in_words"] <= quote_length:
                break
            else:
                continue
        else:
            break

    # Print text and author, with formatting.
    text = q["text"]
    author = q['author']
    print(f'\n"{YW}{text}{RST}" \n- {RED}{author}')


# Call function
get_quote_by_length()
