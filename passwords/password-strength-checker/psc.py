#!/usr/bin/env python3

import os
import string
import argparse

parser = argparse.ArgumentParser (
    description="Password strength checker using password wordlist.",
    epilog="gitlab: https://gitlab.com/tcincere"
)

parser.add_argument(
    "-w", "--wordlist",
    type=str,
    help="Add wordlist to check if password has already been breached.",    
    required=False
)

args = parser.parse_args()
wordlist = args.wordlist    

digit_chars = 0
space_chars = 0
special_chars = 0
lowercase_chars = 0
uppercase_chars = 0

# Get password
password = input("password: ")

# Get absolute path of file
def get_abs_path(wordlist):
    wordlist_abs_path = os.path.abspath(wordlist)

    return wordlist_abs_path

# Check password in passed wordlist
def check_password_breached(password):
    wordlist_path = get_abs_path(wordlist)
    with open(wordlist, 'r') as file:
        breached_passwords = file.read().splitlines()

    if password in breached_passwords:
        return True
 
# First check password in wordlist.
if not wordlist:
    print(f"\n[!] Password not checked in wordlist; it may have been breached.")
else:
    password_breached = check_password_breached(password)
    wordlist_path = get_abs_path(wordlist)
    print(f"Wordlist: {wordlist_path}")

    if password_breached == True:
        print("[!] Password breached. Use another.")
        exit(0)

password_length = len(password)

def check_password_length(password_length):

    global password_length_description
    
    if password_length <= 8:
        password_length_description = "Extremely weak."

    if password_length in range(9, 14):
        password_length_description = "Weak."

    elif password_length in range(13, 21):
        password_length_description = "Probably okay."

    elif password_length in range(20, 41):
        password_length_description = "Probably decent."

    elif password_length in range(40, 65):
        password_length_description = "Probably good."

    elif password_length > 64:
        password_length_description = "Probably great."
        
# Call password length check function.
check_password_length(password_length)

# Check character types
for character in password:

    if character.isupper():
        uppercase_chars += 1

    elif character.islower():
        lowercase_chars += 1

    elif character in string.punctuation:
        special_chars += 1

    elif character.isdigit():
        digit_chars += 1

    elif character.isspace():
        space_chars += 1

# Print information
print(f"\nCharacter types")
print(f"Uppercase char(s):\t{uppercase_chars}")
print(f"Lowercase char(s):\t{lowercase_chars}")
print(f"Digit char(s):    \t{digit_chars}")
print(f"Special char(s):  \t{special_chars}")
print(f"Space char(s):    \t{space_chars}")

print(f"\nLength")
print(f"Passsword length: \t{password_length}")
print(f"Length strength:  \t{password_length_description}")
   
